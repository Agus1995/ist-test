## Configure Spring Datasource, JPA, App properties
Open `src/main/resources/application.properties`
```
spring.datasource.url= jdbc:postgresql://localhost:5432/ist
spring.datasource.username= postgres
spring.datasource.password= 1234

spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation= true
spring.jpa.properties.hibernate.dialect= org.hibernate.dialect.PostgreSQLDialect

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto= update

# App Properties
jwtSecret= agusSecretKey
jwtExpirationMs= 86400000

baseUrlPeople= https://swapi.py4e.com/api/people/
```


## Run Spring Boot application
```
mvn spring-boot:run
```

## Testing
```
Import postman collection IST.postman_collection.json
```