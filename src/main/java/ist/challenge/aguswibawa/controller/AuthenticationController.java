package ist.challenge.aguswibawa.controller;


import ist.challenge.aguswibawa.dto.request.LoginRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.dto.response.CommonResponse;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/signup")
    public ResponseEntity<CommonResponse<SignupRequest>> signup(@RequestBody SignupRequest signupRequest) throws UsernameAlreadyUseException {
        CommonResponse<SignupRequest> response = new CommonResponse<>();
        SignupRequest dataRequest = authenticationService.signup(signupRequest);
        response.setData(dataRequest);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    @PostMapping("/login")
    public CommonResponse<Map<String, String>> login(@RequestBody LoginRequest loginRequest){
        CommonResponse<Map<String, String>> response = new CommonResponse<>();
        Map<String, String> token = authenticationService.login(loginRequest);
        response.setData(token);
        return response;
    }
}
