package ist.challenge.aguswibawa.controller;

import ist.challenge.aguswibawa.dto.request.ListRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.dto.response.CommonResponse;
import ist.challenge.aguswibawa.exception.InvalidDataException;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.model.User;
import ist.challenge.aguswibawa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {


    @Autowired
    private UserService userService;

    @PutMapping("/update")
    public CommonResponse<User> update(@RequestBody SignupRequest user) throws UsernameAlreadyUseException, InvalidDataException {
        CommonResponse<User> response = new CommonResponse<>();
        User dataRequest = userService.update(user);
        response.setData(dataRequest);
        return response;
    }


    @GetMapping("/getList")
    public CommonResponse<Page<User>> getList(@RequestParam("page") int page, @RequestParam("size") int size)  {
        CommonResponse<Page<User>> response = new CommonResponse<>();
        Page<User> users = userService.getListUser(new ListRequest(page-1, size));
        response.setData(users);
        return response;
    }

}
