package ist.challenge.aguswibawa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import ist.challenge.aguswibawa.dto.response.CommonResponse;
import ist.challenge.aguswibawa.exception.ApiFailedException;
import ist.challenge.aguswibawa.exception.InvalidDataException;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.model.User;
import ist.challenge.aguswibawa.service.PeopleService;
import ist.challenge.aguswibawa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/people")
public class PeopleController {


    @Autowired
    private PeopleService peopleService;


    @GetMapping("/list")
    public CommonResponse<Object> list(@RequestParam("search") String search) throws ApiFailedException, JsonProcessingException {
        CommonResponse<Object> response = new CommonResponse<>();
        response.setData(peopleService.getListPeople(search));
        return response;
    }
}
