package ist.challenge.aguswibawa.service;




import ist.challenge.aguswibawa.dto.request.LoginRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;

import java.util.Map;

public interface AuthenticationService {

    SignupRequest signup(SignupRequest signupRequest) throws UsernameAlreadyUseException;

    Map<String, String> login(LoginRequest loginRequest);


}
