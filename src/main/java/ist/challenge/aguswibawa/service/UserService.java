package ist.challenge.aguswibawa.service;

import ist.challenge.aguswibawa.dto.request.ListRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.exception.InvalidDataException;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.model.User;
import org.springframework.data.domain.Page;

public interface UserService {
    User update(SignupRequest signupRequest) throws UsernameAlreadyUseException, InvalidDataException;
    Page<User> getListUser(ListRequest listRequest);
}
