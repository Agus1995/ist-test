package ist.challenge.aguswibawa.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ist.challenge.aguswibawa.exception.ApiFailedException;

public interface PeopleService {
    Object getListPeople(String search) throws ApiFailedException, JsonProcessingException;
}
