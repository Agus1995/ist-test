package ist.challenge.aguswibawa.service.impl;

import ist.challenge.aguswibawa.dto.request.ListRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.exception.InvalidDataException;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.model.User;
import ist.challenge.aguswibawa.repository.UserRepository;
import ist.challenge.aguswibawa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public User update(SignupRequest signupRequest) throws UsernameAlreadyUseException, InvalidDataException {
        User user = new User(
                signupRequest.getId(), signupRequest.getUsername(), signupRequest.getEmail(), signupRequest.getPassword()
        );

        Optional<User> userOptional = userRepository.findByUsername(user.getUsername());
        if (userOptional.isPresent()){
            if (!Objects.equals(userOptional.get().getId(), user.getId())){
                throw new UsernameAlreadyUseException(false, "Username sudah terpakai");
            }
            user.setPassword(encoder.encode(user.getPassword()));
            return userRepository.save(user);
        }
        throw new InvalidDataException(false, "Invalid Data");
    }

    @Override
    public Page<User> getListUser(ListRequest listRequest) {
        PageRequest pageRequest = PageRequest.of(listRequest.getPage(), listRequest.getSize());
        return userRepository.findAll(pageRequest);
    }
}
