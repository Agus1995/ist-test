package ist.challenge.aguswibawa.service.impl;


import ist.challenge.aguswibawa.dto.request.LoginRequest;
import ist.challenge.aguswibawa.dto.request.SignupRequest;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import ist.challenge.aguswibawa.model.User;
import ist.challenge.aguswibawa.repository.UserRepository;
import ist.challenge.aguswibawa.security.jwt.JwtUtils;
import ist.challenge.aguswibawa.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Override
    public SignupRequest signup(SignupRequest signupRequest) throws UsernameAlreadyUseException {
        User user = new User(signupRequest.getUsername(),
                signupRequest.getEmail(),
                encoder.encode(signupRequest.getPassword())
        );

        Optional<User> usr = userRepository.findByUsername(signupRequest.getUsername());

        if (usr.isPresent()){
            throw new UsernameAlreadyUseException(false, "Username sudah terpakai");
        }

        userRepository.save(user);
        return signupRequest;
    }

    @Override
    public Map<String, String> login(LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        Map<String, String> response = new HashMap<>();
        response.put("access_token", jwt);
        return response;
    }
}
