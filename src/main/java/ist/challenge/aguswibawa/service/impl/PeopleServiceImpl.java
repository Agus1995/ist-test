package ist.challenge.aguswibawa.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.aguswibawa.exception.ApiFailedException;
import ist.challenge.aguswibawa.service.PeopleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class PeopleServiceImpl implements PeopleService {


    @Value("${baseUrlPeople}")
    private String baseUrlPeople;


    @Override
    public Object getListPeople(String search) throws ApiFailedException, JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(baseUrlPeople + "?search=" + search, String.class);
        if (!response.getStatusCode().equals(HttpStatus.OK)){
            throw new ApiFailedException(false, "failed get api");
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response.getBody(), Map.class);
    }
}
