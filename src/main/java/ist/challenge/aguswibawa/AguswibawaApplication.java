package ist.challenge.aguswibawa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AguswibawaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AguswibawaApplication.class, args);
	}

}
