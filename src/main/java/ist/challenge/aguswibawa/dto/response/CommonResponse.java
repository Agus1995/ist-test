package ist.challenge.aguswibawa.dto.response;

public class CommonResponse<T> {
    private boolean status;
    private String message;
    private T data;

    public CommonResponse(){
        this.status= true;
        this.message= "success";
    }
    public CommonResponse(boolean status, String message){
        this.status = status;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
