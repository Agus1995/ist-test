package ist.challenge.aguswibawa.exception.handler;

import ist.challenge.aguswibawa.dto.response.CommonResponse;
import ist.challenge.aguswibawa.exception.ApiFailedException;
import ist.challenge.aguswibawa.exception.InvalidDataException;
import ist.challenge.aguswibawa.exception.UsernameAlreadyUseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(value = UsernameAlreadyUseException.class)
    public ResponseEntity<?> resp(UsernameAlreadyUseException e){
        return new ResponseEntity<>(new CommonResponse(e.isStatus(), e.getDescription()), HttpStatus.CONFLICT);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = InvalidDataException.class)
    public ResponseEntity<?> resp(InvalidDataException e){
        return new ResponseEntity<>(new CommonResponse(e.isStatus(), e.getDescription()), HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = ApiFailedException.class)
    public ResponseEntity<?> resp(ApiFailedException e){
        return new ResponseEntity<>(new CommonResponse(e.isStatus(), e.getDescription()), HttpStatus.BAD_REQUEST);
    }
}
