package ist.challenge.aguswibawa.exception;

public class UsernameAlreadyUseException extends Exception {
    private boolean status;
    private String description;

    public UsernameAlreadyUseException(boolean status, String description){
        this.status = status;
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
